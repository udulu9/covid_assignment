<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;

class covidController extends Controller
{
    public function index($selected)
    {
        $getData = Http::get('https://api.covid19api.com/summary');
        $data_json = json_decode($getData);

        $getCase = Http::get('https://api.covid19api.com/dayone/country/' . $selected . '/status/confirmed');
        $case_json = json_decode($getCase);

        //print_r($case_json);

        //charts

        $labels = array();
        $datas = array();
        if ($selected != "global") {
            $days = 30;
            $l = 0;
            $l = sizeof($case_json);
            if ($l - $days > 0) {
                $i = $l - $days;
            }
            for ($i; $i < $l; $i++) {
                $str = substr($case_json[$i]->Date, 0, 10);
                $cases = $case_json[$i]->Cases;
                array_push($labels, $str);
                array_push($datas, $cases);
            }
        }

        $chartjs = app()->chartjs
            ->name('lineChartTest')
            ->type('line')
            ->size(['width' => 400, 'height' => 200])
            ->labels($labels)
            ->datasets([
                [
                    "label" => "Covid Day One Cases",
                    'backgroundColor' => "rgba(38, 185, 154, 0.31)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => $datas,
                ],
            ])
            ->options([]);

        $data = array();
        if ($selected != "global") {
            foreach ($data_json->Countries as $country) {
                if ($country->Slug == $selected) {
                    $data["selected"] = $country;
                }

            }
        } else {
            $data["selected"] = $data_json->Global;
        }

        $data["data"] = $data_json;

        $data["chart"] = $chartjs;

        return view('dashboard', compact('data'));
    }
}
