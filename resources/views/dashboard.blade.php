<header>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    <div class="row justify-content-center">
        <h1 class="display-4 mt-2">
            Dashboard
        </h1>
    </div>

</header>

<style>
    .dropdown-menu {
        max-height: 200px;
        overflow-y: auto;
    }

</style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.2.0/chart.min.js"></script>

<body>
    <div class="container">
        <div class="row mt-4">

            <div class="col-md-4">
                <div class="media">
                    <img class="mr-3" src="..." alt="">
                    <div class="media-body">
                        @if (isset($data['selected']->Slug))
                            <h5 id="Country" class="mt-0"><span
                                    style="text-transform:uppercase">{{ $data['selected']->Slug }}</span></h5>
                        @else
                            <h5 id="Country" class="mt-0">GLOBAL</h5>
                        @endif


                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" id="dropdownMenuButton" type="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Select Country
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                @if (isset($data))
                                    <a class="dropdown-item" href="../covid/global">Global</a>
                                    @foreach ($data['data']->Countries as $country)
                                        <a class="dropdown-item"
                                            href="../covid/{{ $country->Slug }}">{{ $country->Slug }}</a>
                                    @endforeach
                                @ENDIF
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="media">
                    <img class="mr-3" src="..." alt="">
                    <div class="media-body">
                        <h5 id="new_confirmed" class="mt-0">New Confirmed</h5>
                        @if (isset($data))
                            @php
                                echo $data['selected']->NewConfirmed;
                            @endphp
                        @ENDIF
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="media">
                    <img class="mr-3" src="..." alt="">
                    <div class="media-body">
                        <h5 id="total_confirmed" class="mt-0">Total Confirmed</h5>
                        @if (isset($data))
                            @php
                                echo $data['selected']->TotalConfirmed;
                            @endphp
                        @ENDIF
                    </div>
                </div>
            </div>

        </div>

        <div class="row mt-4">

            <div class="col-md-4">
                <div class="media">
                    <img class="mr-3" src="..." alt="">
                    <div class="media-body">
                        <h5 id="total_deaths" class="mt-0">Total Deaths</h5>
                        @if (isset($data))
                            @php
                                echo $data['selected']->TotalDeaths;
                            @endphp
                        @ENDIF
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="media">
                    <img class="mr-3" src="..." alt="">
                    <div class="media-body">
                        <h5 id="new_recovered" class="mt-0">New Recovered</h5>
                        @if (isset($data))
                            @php
                                echo $data['selected']->NewRecovered;
                            @endphp
                        @ENDIF
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="media">
                    <img class="mr-3" src="..." alt="">
                    <div class="media-body">
                        <h5 id="total_recovered" class="mt-0">Total Recovered</h5>
                        @if (isset($data))
                            @php
                                echo $data['selected']->TotalRecovered;
                            @endphp
                        @ENDIF
                    </div>
                </div>
            </div>

        </div>

        <div class="row">

            {!! $data["chart"]->render() !!}

        </div>

    </div>

</body>
